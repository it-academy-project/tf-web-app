resource "aws_security_group" "wp_security_group" {
  name = "${var.app_name}_security_group"
  description = "Security group of WEB app"

  ingress {
    from_port = var.server_port
    protocol = "tcp"
    to_port = var.server_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.ssh_access_port
    protocol = "tcp"
    to_port = var.ssh_access_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = -1
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name": "${var.app_name}_security_group"
  }
}