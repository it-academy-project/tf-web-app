provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "wp_app" {
    ami = var.ami_id
    instance_type = var.instance_type
    key_name = var.access_key_name

    vpc_security_group_ids = [aws_security_group.wp_security_group.id]
    tags = {
        "Name": "${var.environment_name}_${var.app_name}"
        "Release": var.app_release_version
    }
}