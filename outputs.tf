output "public_ip" {
  value = aws_instance.wp_app.public_ip
  description = "The Public IP address of the Django App"
}