variable "server_port" {
  description = "HTTP server port"
  type = number
  default = 8080
}

variable "ssh_access_port" {
  description = "SSH access port"
  type = number
  default = 22
}

variable "environment_name" {
  description = "Environment variable name"
  type = string
  default = "stage"
}

variable "app_name" {
  description = "WEB application name"
  type = string
  default = "wp_web_app"
}

variable "app_release_version" {
  description = "WEB application release version"
  type = string
  default = "v01"
}

variable "ami_id" {
  description = "AMI ID string (Ubuntu 18.04 (64-bit x86))"
  type = string
  default = "ami-0e1ce3e0deb8896d2"
}

variable "instance_type" {
  description = "EC2 instance type"
  type = string
  default = "t2.micro"
}

variable "access_key_name" {
  description = "Access key name"
  type = string
  default = "ekylevski_devops"
}
